<?php

namespace App\Http\Requests;


class EmployeeRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:employees,email,' . $this->employee,
            'max_restaurant_assigned' => 'required|integer|min:1',
            'restaurants' => 'array|nullable',
            'restaurants.*' => 'integer|exists:restaurants,id',
        ];
    }
}
