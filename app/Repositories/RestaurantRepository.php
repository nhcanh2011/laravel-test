<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Collection;

class RestaurantRepository extends BaseRepository
{
    public function getRestaurants(): Collection
    {
        return Restaurant::query()->orderBy('name')->get();
    }

    public function getRestaurantEmployees(int $restaurantId): Collection
    {
        return Employee::query()
            ->whereHas('restaurants', function ($query) use ($restaurantId) {
                $query->where('id', $restaurantId);
            })
            ->get();
    }
}
