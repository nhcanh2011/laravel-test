<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class EmployeeRepository extends BaseRepository
{
    public function getEmployee(int $id): ?Employee
    {
        return Employee::query()->find($id);
    }

    public function getEmployees(): Collection
    {
        return Employee::query()
            ->with('restaurants')
            ->orderBy('first_name')
            ->orderBy('last_name')
            ->get();
    }

    public function deleteEmployees(int $id): array|null
    {
        $employee = Employee::query()->find($id);
        if (empty($employee)) {
            return ['error' => 'The employee does not exist'];
        }
        DB::transaction(function () use ($employee) {
            $employee->restaurants()->detach();
            $employee->delete();
        });
        return null;
    }

    public function createEmployee(array $args): array|null
    {
        $restaurants = [];
        if (!empty($args['restaurants'])) {
            $restaurants = $args['restaurants'];
            unset($args['restaurants']);

            $restaurantsHasMaxEmployee = $this->getRestaurantsHasMaxEmployee($restaurants);
            if (!empty($restaurantsHasMaxEmployee)) {
                return ['error' => 'Restaurants Have Max Employee : ' . implode(', ', array_column($restaurantsHasMaxEmployee, 'name'))];
            }
        }
        DB::transaction(function () use ($args, $restaurants) {
            $employee =  Employee::query()->create($args);
            if (!empty($restaurants)) {
                $this->assignEmployeeRestaurants($employee, $restaurants);
            }
        });
        return null;
    }

    public function updateEmployee(int $id, array $args): array|null
    {
        $employee = Employee::query()->with('restaurants')->find($id);
        if (empty($employee)) {
            return ['error' => 'The employee does not exist'];
        }
        $assignedToMaxRestaurant = $this->assignedEmployeeToMaxRestaurant($args['max_restaurant_assigned'], $employee->restaurants->count());
        if ($assignedToMaxRestaurant) {
            return ['error' => 'The employee is assigned to the max number of restaurants'];
        }
        $restaurants = [];
        if (!empty($args['restaurants'])) {
            $restaurants = $args['restaurants'];
            unset($args['restaurants']);

            $currentRestaurants = $employee->restaurants->pluck('id')->toArray();
            $assignRestaurants = array_diff($restaurants, $currentRestaurants);
            $restaurantsHasMaxEmployee = $this->getRestaurantsHasMaxEmployee($assignRestaurants);
            if (!empty($restaurantsHasMaxEmployee)) {
                return ['error' => 'Restaurants Have Max Employee : ' . implode(', ', array_column($restaurantsHasMaxEmployee, 'name'))];
            }
        }
        DB::transaction(function () use ($args, $restaurants, $employee) {
            $employee->update($args);
            $this->assignEmployeeRestaurants($employee, $restaurants);
        });
        return null;
    }

    protected function assignEmployeeRestaurants(Employee $employee, array $restaurants): void
    {
        $currentRestaurants = $employee->restaurants->pluck('id')->toArray();
        $unAssignedRestaurants = array_diff($currentRestaurants, $restaurants);
        if (!empty($unAssignedRestaurants)) {
            $employee->restaurants()->detach($unAssignedRestaurants);
        }
        $assignRestaurants = array_diff($restaurants, $currentRestaurants);
        if (!empty($assignRestaurants)) {
            $employee->restaurants()->attach($restaurants);
        }
    }

    protected function assignedEmployeeToMaxRestaurant(int $maxRestaurantAssigned, int $countEmployeeRestaurants): bool
    {
        return $maxRestaurantAssigned < $countEmployeeRestaurants;
    }

    protected function getRestaurantsHasMaxEmployee(array $restaurantIds): array
    {
        $result = [];
        $restaurants = Restaurant::query()
            ->withCount(['employees'])
            ->whereIn('id', $restaurantIds)
            ->get();
        foreach ($restaurants as $restaurant) {
            if ($restaurant->max_employee_amount <= $restaurant->employees_count) {
                $result[] = $restaurant;
            }
        }
        return $result;
    }
}
