<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeRestaurant extends Model
{
    use HasFactory;

    protected $table = 'employee_restaurant';

    protected $primaryKey = null;

    public $timestamps = false;
}
