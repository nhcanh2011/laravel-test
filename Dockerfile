FROM php:8.2-cli
LABEL authors="Mr Canh"

RUN apt-get update && apt-get install -y libpq-dev git zip npm && docker-php-ext-install pdo pdo_mysql

COPY --from=composer /usr/bin/composer /usr/local/bin/composer

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
