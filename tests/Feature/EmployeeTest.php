<?php

namespace Tests\Feature;

use App\Models\Employee;
use App\Models\Restaurant;
use App\Models\User;
use Tests\TestCase;

class EmployeeTest extends TestCase
{
    public function test_employee_api_without_logged(): void
    {
        $response = $this->get('/api/employee');

        $response->assertRedirect('login');
    }

    public function test_employee_api_with_logged(): void
    {
        $user = $this->getLoggedUser();
        $response = $this->actingAs($user)->get('/api/employee');
        $response->assertStatus(200);
    }

    public function test_get_employee_api_with_wrong_id(): void
    {
        $user = $this->getLoggedUser();
        $response = $this->actingAs($user)->get('/api/employee/x');
        $response->assertStatus(500);

        $user = $this->getLoggedUser();
        $response = $this->actingAs($user)->get('/api/employee/1.2');
        $response->assertStatus(200);

        $user = $this->getLoggedUser();
        $response = $this->actingAs($user)->get('/api/employee/9999999');
        $response->assertStatus(200)->assertContent('');
    }

    public function test_get_employee_api_with_good_id(): void
    {
        $user = $this->getLoggedUser();
        $employee = Employee::query()->first();
        $response = $this->actingAs($user)->get('/api/employee/' . $employee->id);
        $response->assertStatus(200)->assertContent($employee->toJson());
    }

    public function test_create_employee_api_with_wrong_param(): void
    {
        $user = $this->getLoggedUser();
        //missed param
        $response = $this->actingAs($user)
            ->post('/api/employee/');
        $response->assertStatus(403)
            ->assertSeeText('error');

        //wrong email
        $response = $this->actingAs($user)
            ->post('/api/employee/', [
                'first_name' => fake()->firstName(),
                'last_name' => fake()->firstName(),
                'email' => fake()->paragraph(),
                'max_restaurant_assigned' => random_int(1, 10),
            ]);
        $response->assertStatus(403)
            ->assertSeeText('error');

        //unique email
        $employee = Employee::query()->first();
        $response = $this->actingAs($user)
            ->post('/api/employee/', [
                'first_name' => fake()->firstName(),
                'last_name' => fake()->firstName(),
                'email' => $employee->email,
                'max_restaurant_assigned' => random_int(1, 10),
            ]);
        $response->assertStatus(403)
            ->assertSeeText('error');

        //wrong max_restaurant_assigned
        $response = $this->actingAs($user)
            ->post('/api/employee/', [
                'first_name' => fake()->firstName(),
                'last_name' => fake()->firstName(),
                'email' => fake()->email(),
                'max_restaurant_assigned' => fake()->randomElement([-1, 0, 'xxxx']),
            ]);
        $response->assertStatus(403)
            ->assertSeeText('error');

        //wrong restaurants
        $response = $this->actingAs($user)
            ->post('/api/employee/', [
                'first_name' => fake()->firstName(),
                'last_name' => fake()->firstName(),
                'email' => fake()->email(),
                'max_restaurant_assigned' => random_int(1, 10),
                'restaurants' => [9999999, 1]
            ]);
        $response->assertStatus(403)
            ->assertSeeText('error');
    }

    public function test_create_employee_api_with_good_param(): void
    {
        $user = $this->getLoggedUser();

        //without restaurants
        $response = $this->actingAs($user)
            ->post('/api/employee/', [
                'first_name' => fake()->firstName(),
                'last_name' => fake()->firstName(),
                'email' => fake()->email(),
                'max_restaurant_assigned' => random_int(1, 10),
            ]);
        $response->assertSuccessful();


        //with restaurants
        $restaurants = Restaurant::query()->limit(2)->get()->pluck('id')->toArray();
        $response = $this->actingAs($user)
            ->post('/api/employee/', [
                'first_name' => fake()->firstName(),
                'last_name' => fake()->firstName(),
                'email' => fake()->email(),
                'max_restaurant_assigned' => random_int(1, 10),
                'restaurants' => $restaurants
            ]);
        $response->assertSuccessful();
    }

    public function test_update_employee_api_with_wrong_param(): void
    {
        $user = $this->getLoggedUser();
        $employee = Employee::query()->first();
        $url = '/api/employee/' . $employee->id;
        //missed param
        $response = $this->actingAs($user)
            ->put($url);
        $response->assertStatus(403)
            ->assertSeeText('error');

        //wrong email
        $response = $this->actingAs($user)
            ->put($url, [
                'first_name' => fake()->firstName(),
                'last_name' => fake()->firstName(),
                'email' => fake()->paragraph(),
                'max_restaurant_assigned' => random_int(1, 10),
            ]);
        $response->assertStatus(403)
            ->assertSeeText('error');

        //unique email
        $employee2 = Employee::query()->orderByDesc('id')->first();
        $response = $this->actingAs($user)
            ->put($url, [
                'first_name' => fake()->firstName(),
                'last_name' => fake()->firstName(),
                'email' => $employee2->email,
                'max_restaurant_assigned' => random_int(1, 10),
            ]);
        $response->assertStatus(403)
            ->assertSeeText('error');

        //wrong max_restaurant_assigned
        $response = $this->actingAs($user)
            ->put($url, [
                'first_name' => fake()->firstName(),
                'last_name' => fake()->firstName(),
                'email' => fake()->email(),
                'max_restaurant_assigned' => fake()->randomElement([-1, 0, 'xxxx']),
            ]);
        $response->assertStatus(403)
            ->assertSeeText('error');

        //wrong restaurants
        $response = $this->actingAs($user)
            ->put($url, [
                'first_name' => fake()->firstName(),
                'last_name' => fake()->firstName(),
                'email' => fake()->email(),
                'max_restaurant_assigned' => random_int(1, 10),
                'restaurants' => [9999999, 1]
            ]);
        $response->assertStatus(403)
            ->assertSeeText('error');
    }

    public function test_update_employee_api_with_good_param(): void
    {
        $user = $this->getLoggedUser();
        $employee = Employee::query()->first();
        $url = '/api/employee/' . $employee->id;

        //without restaurants
        $response = $this->actingAs($user)
            ->put($url, [
                'first_name' => fake()->firstName(),
                'last_name' => fake()->firstName(),
                'email' => fake()->email(),
                'max_restaurant_assigned' => random_int(1, 10),
            ]);
        $response->assertSuccessful();


        //with restaurants
        $restaurants = Restaurant::query()->limit(2)->get()->pluck('id')->toArray();
        $response = $this->actingAs($user)
            ->put($url, [
                'first_name' => fake()->firstName(),
                'last_name' => fake()->firstName(),
                'email' => fake()->email(),
                'max_restaurant_assigned' => random_int(1, 10),
                'restaurants' => $restaurants
            ]);
        $response->assertSuccessful();
    }

    public function test_delete_employee_api_with_wrong_param(): void
    {
        $user = $this->getLoggedUser();
        $url = '/api/employee/' . fake()->randomElement([-1, 0, 'xxxx']);
        $response = $this->actingAs($user)->delete($url);
        $response->assertSeeText('error');
    }

    public function test_delete_employee_api_with_good_param(): void
    {
        $user = $this->getLoggedUser();
        $employee = Employee::query()->first();
        $url = '/api/employee/' . $employee->id;
        $response = $this->actingAs($user)->delete($url);
        $response->assertSuccessful();
    }

    protected function getLoggedUser()
    {
        return User::query()->first();
    }

}
