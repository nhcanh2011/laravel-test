## The project is built on (see docker-compose.yml file)
### php8.2-cli
### laravel version 11.x: https://laravel.com/docs/11.x/
### mysql latest version

## I. Steps to run:
### 1. run this command to create the network
    docker network create --subnet=172.28.0.0/16 laravel_test
### 2. run this commande in current project path
    docker compose up -d
    
## II. Tests
### 1. login page: http://127.0.0.1:8000/login
login with the account: *admin@test.com*, pass: *123456*
### 2. test page: http://127.0.0.1:8000/dashboard
### 5. the feature test at tests/Feature/EmployeeTest.php, how to run test
* go into the code container by command: *docker exec -it test_code bash*
* run this command and see the log: *php artisan test*
