<?php

namespace Database\Seeders;

use App\Models\Employee;
use App\Models\Restaurant;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()->create();
        Restaurant::factory()
            ->state(new Sequence(
                ['name' => 'MeatChefs'],
            ))
            ->has(Employee::factory(random_int(3, 5)))
            ->create();
        Restaurant::factory()
            ->state(new Sequence(
                ['name' => 'VegeChefs'],
            ))
            ->has(Employee::factory(random_int(3, 5)))
            ->create();
        Restaurant::factory()
            ->state(new Sequence(
                ['name' => 'BurgerChefs'],
            ))
            ->has(Employee::factory(random_int(3, 5)))
            ->create();
    }
}
